function countLetter(letter, sentence) {
  let result = 0;

  // Check if letter is a single character
  if (letter.length === 1) {
    for (let i = 0; i < sentence.length; i++) {
      if (sentence[i] === letter) {
        result++;
      }
    }
    return result;
  } else {
    // If letter is not a single character, return undefined
    return undefined;
  }
}
let sentence = "The quick brown fox jumps over the lazy dog";
let letter = "o";

let count = countLetter(letter, sentence);
console.log(`The letter "${letter}" appears ${count} times.`);

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    text = text.toLowerCase();
    let letterCounts = {};

      for (let i = 0; i < text.length; i++) {
    let letter = text[i];

    if (letterCounts[letter]) {
      return false;
    }
        letterCounts[letter] = 1;
  }

    // If the function finds a repeating letter, return false. Otherwise, return true.
 return true;
    
}

let text1 = "Hello";
let isIsogram1 = isIsogram(text1);
console.log(`${text1} is an isogram: ${isIsogram1}`);
const text2 = "World";
const isIsogram2 = isIsogram(text2);
console.log(`${text2} is an isogram: ${isIsogram2}`);

function purchase(age, price) {
    // Return undefined for people aged below 13
    if (age < 13) {
      return undefined;
    }
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    if (age >= 13 && age <= 21 || age >= 65) {
      const discountedPrice = price * 0.8;
      return Math.round(discountedPrice).toString();
    }
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
      return Math.round(price).toString();
    }

const price1 = 100;
const age1 = 10;
const result1 = purchase(age1, price1);
console.log(`The price for a ${age1}-year-old is ${result1}.`);

const price2 = 100;
const age2 = 20;
const result2 = purchase(age2, price2);
console.log(`The price for a ${age2}-year-old is ${result2}.`);

const price3 = 100;
const age3 = 30;
const result3 = purchase(age3, price3);
console.log(`The price for a ${age3}-year-old is ${result3}.`);


function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};

let x = 5
function double(num){
  let x = num * 2;
  return x;
}