
// import Button from 'react-bootstrap/Button';
// // Bootstrap grid system components
// import { Row } from 'react-bootstrap/Row';
// import { Col } from 'react-bootstrap/Col';


import React from 'react';
import { Button, Row, Col } from 'react-bootstrap';

export default function Banner({ title, subtitle, buttonText, link }) {
  return (
    <Row>
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{subtitle}</p>
        <Button variant="primary" href={link}>
          {buttonText}
        </Button>
      </Col>
    </Row>
  );
}