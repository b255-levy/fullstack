import React from 'react';

//Creates a context object
// A context object as the name state is a data type of an object that can be used to store information that can be shared to other components within the app
// The context object is a different approach to passing information between components and alows easier access by avoiding the use of prop-drilling

const UserContext = React.createContext();

// The "Provider" component allows other component to consume/use the context object and supply the necessary information needed

export const UserProvider = UserContext.Provider;

export default UserContext;