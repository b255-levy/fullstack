import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import React from 'react';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Home from './pages/Home';
import CourseCard from './components/CourseCard';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import './App.css';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';

function App() {
  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the local storage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })
  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

//Used to check if the user info is propery stored on login
  useEffect(() => {
    console.log(user);
    console.log(localStorage)
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <Container fluid>
        <AppNavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/coursesView/:courseId" element={<CourseView />} />
          <Route path="/Login" element={<Login />} />
          <Route path="/Logout" element={<Logout />} />
          <Route path="/Register" element={<Register />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;

// Create Route paths to the Course component, Register component, and login component