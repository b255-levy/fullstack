import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout(){
	// Consume the UserContext object and destructure it to acces the user state and the unsetUser function from the context provider
	const {unsetUser, setUser} = useContext(UserContext);

	//Clear the localStoarage of the user's information
	unsetUser();
	
	// Placing the "setUser" setter function inside of a userEffect is necessary because of updates within React JS that a state of another component cannot be updated while trying to render a different component
	//By adding the useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of the user

	useEffect(() => {
		// Sets the user state back to its orgiinal value
		setUser({id:null});
	})
	return(
		<Navigate to='/login'/>
		)
}