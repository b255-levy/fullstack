import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Register(){
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (user.email) {
            navigate('/courses');
        }
    }, [user, navigate]);


// State hooks to store the values of the input fields.
	const [email, setEmail] = useState()
	const [password1, setPassword1] = useState()
	const [password2, setPassword2] = useState()
// State tp determine whether submit button is enabled or not
	const [isActive, setIsActive ] = useState('')
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	useEffect(() => {
	  if (
	    firstName !== '' &&
	    lastName !== '' &&
	    email !== '' &&
	    password1 !== '' &&
	    password2 !== '' &&
	    mobileNumber.length >= 11 &&
	    password1 === password2
	  ) {
	    setIsActive(true);
	  } else {
	    setIsActive(false);
	  }
	}, [firstName, lastName, email, password1, password2, mobileNumber]);



	console.log(email);
	console.log(password1);
	console.log(password2);

	async function submitFunction(user) {
	  try {
	    const response = await fetch('http://localhost:4000/users/register', {
	      method: 'POST',
	      headers: {
	        'Content-Type': 'application/json',
	      },
	      body: JSON.stringify(user),
	    });

	    if (response.ok) {
	      const data = await response.json();

	            if (data.success) {
	              return true;
	            } else {
	              // Handle server-side validation error messages
	              Swal.fire({
	                icon: 'error',
	                title: 'Error',
	                text: data.message,
	              });
	              return false;
	            }
	          } else {
	            Swal.fire({
	              icon: 'error',
	              title: 'Error',
	              text: 'Registration failed. Please try again later.',
	            });
	            return false;
	          }
	        } catch (error) {
	          console.error(error);
	          Swal.fire({
	            icon: 'error',
	            title: 'Error',
	            text: 'An error occurred while registering. Please try again later.',
	          });
	          return false;
	        }
	      }


		function registerUser(e){
			// Prevents page redirection via form submission
			e.preventDefault();

			//Clear input fields
			  try {
			    if (submitFunction({ firstName, lastName, email, mobileNumber, password: password1 })) {
			      // Clear input fields
			      setFirstName('');
			      setLastName('');
			      setMobileNumber('');
			      setEmail('');
			      setPassword1('');
			      setPassword2('');

			      Swal.fire({
			        icon: 'success',
			        title: 'Thank you for registering',
			      });
			      
			      navigate('/login');
			    }
			  } catch (error) {
			    Swal.fire({
			      icon: 'error',
			      title: 'Error',
			      text: error.message,
			    });
			  }
			}

		useEffect(() => {

			// validation to enable submit button when all fields are populated and both passwords match
			if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
				setIsActive(true);
			} else {
				setIsActive(false);
			}

		}, [email, password1, password2]);

	return(

		(user.id !== null) ? 
			<navigate to="/courses"/>
			:
		<Form onSubmit={(e) => registerUser(e)}>
		                    <Form.Group controlId="userEmail">
		                        <Form.Label>Email address</Form.Label>
		                        <Form.Control 
		                            type="email" 
		                            placeholder="Enter email" 
		                            value={email}
		                            onChange={e => setEmail(e.target.value)}
		                            required
		                        />
		                        <Form.Text className="text-muted">
		                            We'll never share your email with anyone else.
		                        </Form.Text>
		                    </Form.Group>

		                    <Form.Group controlId="password1">
		                        <Form.Label>Password</Form.Label>
		                        <Form.Control 
		                            type="password" 
		                            placeholder="Password" 
		                            value={password1}
		                            onChange={e => setPassword1(e.target.value)}
		                            required
		                        />
		                    </Form.Group>

		                    <Form.Group controlId="password2">
		                        <Form.Label>Verify Password</Form.Label>
		                        <Form.Control 
		                            type="password" 
		                            placeholder="Verify Password"
		                            value={password2} 
		                            onChange={e => setPassword2(e.target.value)}
		                            required
		                        />
		                    </Form.Group>

		                    <Form.Group controlId="firstName">
		                      <Form.Label>First Name</Form.Label>
		                      <Form.Control
		                        type="text"
		                        placeholder="Enter first name"
		                        value={firstName}
		                        onChange={(e) => setFirstName(e.target.value)}
		                        required
		                      />
		                    </Form.Group>

		                    <Form.Group controlId="lastName">
		                      <Form.Label>Last Name</Form.Label>
		                      <Form.Control
		                        type="text"
		                        placeholder="Enter last name"
		                        value={lastName}
		                        onChange={(e) => setLastName(e.target.value)}
		                        required
		                      />
		                    </Form.Group>

		                    <Form.Group controlId="mobileNumber">
		                      <Form.Label>Mobile Number</Form.Label>
		                      <Form.Control
		                        type="tel"
		                        placeholder="Enter mobile number"
		                        value={mobileNumber}
		                        onChange={(e) => setMobileNumber(e.target.value)}
		                        pattern="\d{11}"
		                        required
		                      />
		                      <Form.Text className="text-muted">
		                        Please enter an 11-digit mobile number.
		                      </Form.Text>
		                    </Form.Group>


		                    {isActive ?

		                    <Button variant="primary" type="submit" id="submitBtn">
		                        Submit
		                    </Button>
		                    :
		                    <Button variant="primary" type="submit" id="submitBtn" disabled>
		                        Submit
		                    </Button>
		                   }
		                </Form>
		                )
}