import React from 'react';
import Banner from '../components/Banner';

export default function ErrorPage() {
  return (
    <Banner
      title="404 - Page Not Found"
      subtitle="Oops! The page you are looking for does not exist."
      buttonText="Go Home"
      link="/"
    />
  );
}