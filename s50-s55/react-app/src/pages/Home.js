import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import React from 'react';



export default function Home() {
  return (
    <>
      <Banner
        title="Zuitt Coding Bootcamp"
        subtitle="Opportunities for everyone, everywhere."
        buttonText="Enroll now!"
        link="#enroll"
      />
      <Highlights /> // Add Highlights component to Home page
    </>
  );
}
