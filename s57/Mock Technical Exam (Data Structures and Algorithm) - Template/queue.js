let collection = [];

function print() {
  return collection;
}

function enqueue(item) {
  collection.push(item);
  return collection;
}

function dequeue() {
  return collection.shift();
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return (collection.length === 0);
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};
